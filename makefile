name = cs351-project1

compile : $(name).tex $(name).bib selection.tex assembly.tex evaluation.tex
	pdflatex $(name) && \
	#bibtex $(name) && \
	pdflatex $(name) && \
	pdflatex $(name)

clean :
	rm -f \
	$(name).aux \
	$(name).bbl \
	$(name).blg \
	$(name).log \
	$(name).out \
	$(name).pdf \
	$(name).toc
