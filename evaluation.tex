%45 points.

\subsection{Performance}
%10 points.
%Need metrics---this should be fun.
The most important performance consideration for our client is the ability to run scientific workloads like folding@home. 
We make the assumption that a desktop that can run folding@home proficiently can also handle routine desktop use, such as running a word processor or playing movies and music. 

To evaluate the systems' performance in terms of folding@home, there are many quantities we might consider:
\begin{itemize}
\item \emph{CPU frequency} can be used as a rough measure of a machine's rate of completing small units of work on a single processor core. 
However, CPU frequency alone is inadequate when comparing two machines using CPU's with a different number of cores. 
Even comparing single-core processors, higher CPU frequency does not indicate faster performance for every application.
\item \emph{Number of processor cores} is a useful quantity for comparing two machines' ability to complete work in parallel. 
This number is important for comparing the throughput potential of two machines running parallel workloads, like folding@home.
The two Intel processors we have selected utilize ``hyper-threading'' technology, which means they can process multiple threads on each physical core. 
As part of a metric to predict folding@home throughput, we ignore hyper-threading and use the number of physical processor cores.
\item \emph{Memory speed and bandwidth} would be the best indicator of performance for workloads that ``bottleneck'' at the communication between the CPU and memory. 
However, because it is unclear exactly how important memory speeds are to folding@home workloads, it is hard to know how heavily to weigh this quantity in a performance metric. 
For this reason, we assume that the difference in memory speed and bandwidth across our three systems will not significantly affect performance, and so we do not include it in our metric.
\item \emph{Disk I/O speed} will play a role in computational workloads that require the use of huge amounts of storage. 
We make the assumption that folding@home does not depend heavily on disk I/O, so we do not inlude this quantity in our metric either.
\end{itemize}

If we had included a GPU in one of our systems, we would need to consider a number of quantities related to that GPU as well. 
Since our systems do not incorporate GPU's (see Section~\ref{sec:selection-processor-gpu}), folding@home will depend heavily on the CPU. 

We define a simple metric $M$, based on the CPU, to compare performance across our systems:
\[
M = \left(\textrm{No. of cores}\right)\times\left(\textrm{CPU frequency}\right)
\]
The units of this metric are 
\[
\displaystyle \frac{\textrm{cycles}\cdot\textrm{cores}}{\textrm{second}}.
\]

Including only quantities related to the CPU means that this metric assumes the rest of the system (e.g.\ quantity/speed of available memory) is configured to optimize CPU performance. 
Since adequate support for the CPU was a primary design goal for these systems, we are comfortable making this assumption.

\begin{center}
\begin{tabular}{|l|rrr|}
\cline{2-4}
\multicolumn{1}{l|}{} & System A & System B & System C \\
\hline
No. of cores & 2 & 4 & 8 \\
CPU frequency (GHz) & 3.3 & 3.1 & 4.0 \\
\hline
$M$ & 6.6 & 12.4 & 32.0 \\
\hline
\end{tabular}
\end{center}

Using metric $M$, the speedup of System C over System B is $32.0/12.4\approx 2.6$, and the speedup of System B over System A is $12.4/6.6\approx 1.9$. 
We rank the systems from highest performance to lowest:
\begin{enumerate}
\item System C: $M=32.0$
\item System B: $M=12.4$
\item System A: $M=6.6$
\end{enumerate}

\subsection{Price}
%10 points.
%Need summary tables with price information for each system.
\begin{center}
\begin{tabular}{|l|l|r|r|r|}
\cline{3-5}
\multicolumn{2}{l|}{} & \multicolumn{3}{|c|}{Price} \\
\cline{2-5}
\multicolumn{1}{l|}{}  & Component & System A & System B & System C \\
\hline
\multirow{3}{*}{Processor} & Intel i3-3225 & \$139.99 &  &  \\
 & Intel i5-3350P &  & \$179.99 &  \\
 & AMD FX-8350 &  &  & \$199.99 \\
\hline
\multirow{2}{*}{Memory} & Corsair 2$\times$2 GB & 44.99 &  &  \\
 & Kingston 2$\times$4 GB &  & 92.99 & 92.99 \\
\hline
\multirow{2}{*}{Storage} & WD Caviar Green 1.5 TB & 85.93 & 85.93 &  \\
 & WD Caviar Blue 2$\times$750 GB &  &  & 169.98 \\
\hline
\multirow{2}{*}{Motherboard} & ASRock H77 & 74.99 & 74.99 &  \\
 & GIGABYTE GA-970A-D3P &  &  & 89.99 \\
\hline
Power supply & Apevia ATX-AS520W-BK & 27.99 & 27.99 & 27.99 \\
Case & GIGABYTE GZ-ZA2 & 39.99 & 39.99 & 39.99 \\
Optical drive & ASUS DRW-24F1ST & 19.99 & 19.99 & 19.99 \\
\hline
\multicolumn{1}{l|}{} & \multicolumn{1}{|r|}{Total} & \$433.87 & \$521.87 & \$640.92 \\
\cline{2-5}
\end{tabular}
\end{center}

The tables above summarize price for each system; we rank them from least to most expensive:
\begin{enumerate}
\item System A: \$433.87
\item System B: \$521.87
\item System C: \$640.92
\end{enumerate}

See Section~\ref{sec:selection} for price references.

\subsection{Power Consumption}
\label{sec:evaluation-power}
%10 points.
%Need detailed power estimates for each system.
Because the client intends to run folding@home when not using the computer, the systems will likely consume at the high end of their dynamic power range most of the time. 
For this reason, we evaluate the systems' power consumption based on ``peak'' power, and we do not concern ourselves with idle power.
\begin{center}
\begin{tabular}{|l|l|r|r|r|}
\cline{3-5}
\multicolumn{2}{l|}{} & \multicolumn{3}{|c|}{Power (W)} \\
\cline{2-5}
\multicolumn{1}{l|}{}  & Component & System A & System B & System C \\
\hline
\multirow{3}{*}{Processor} & Intel i3-3225 & 55.0 &  &  \\
 & Intel i5-3350P &  & 69.0 &  \\
 & AMD FX-8350 &  &  & 125.0 \\
\hline
\multirow{2}{*}{Memory} & Corsair 2$\times$2 GB & 10.0 &  &  \\
 & Kingston 2$\times$4 GB &  & 10.0 & 10.0 \\
\hline
\multirow{2}{*}{Storage} & WD Caviar Green 1.5 TB & 6.0 & 6.0 &  \\
 & WD Caviar Blue 2$\times$750 GB &  &  & 16.0 \\
\hline
\multirow{2}{*}{Chipset} & ASRock H77 & 6.7 & 6.7 &  \\
 & GIGABYTE GA-970A-D3P &  &  & 13.6 \\
\hline
Case fans & 1$\times$120mm & 5.0 & 5.0 & 5.0 \\
Optical drive & ASUS DRW-24F1ST & 20.0 & 20.0 & 20.0 \\
\hline
\multicolumn{1}{l|}{} & \multicolumn{1}{|r|}{Total} & 102.7 & 116.7 & 189.6 \\
\cline{2-5}
\multicolumn{1}{l|}{} & \multicolumn{1}{|r|}{$\textrm{Total}/0.8$} & 128.4 & 145.9 & 237.0 \\
\cline{2-5}
\end{tabular}
\end{center}

The power consumption of each system is calculated using the component power numbers in the table above. 
We estimate actual power drawn from the wall by dividing the sum of component power by 0.8 to account for 80\% inefficiency in the power supply. 
Note that we also make the following, hopefully conservative, assumptions about component power consumption:
\begin{itemize}
\item Memory draws 5 W per DIMM.
\item Case fans draw 5 W per fan.
\item The optical drive may draw up to 20 W.
\end{itemize}
Chipset power references:
\begin{itemize}
\item Intel H77: \url{http://en.wikipedia.org/wiki/List_of_Intel_chipsets}
\item AMD 970: \url{http://en.wikipedia.org/wiki/AMD_900_chipset_series}
\end{itemize}

We rank the systems by estimated peak power, from lowest to highest:
\begin{enumerate}
\item System A: 128.4 W
\item System B: 145.9 W
\item System C: 237.0 W
\end{enumerate}

\subsection{Overall Analysis}
%15 points.
%Need overall recommendations by 2 different quantitative methods.
In this section, we use performance, price, and power data to determine our overall recommendation(s).

\subsubsection{Recommendation 1: Green}
The first recommendation we make is based on a measure of how ``green'' each system is. 
We guess that since our client is interested in donating computer resources to medical research \emph{for the greater good}, our client may also be interested in balancing performance against the environmental impact of this computational contribution in terms of electricity usage.
For this recommendation, we ignore price, and define $G$ (higher $G$ means greener):
\[
G = \frac{\textrm{Performance}}{\textrm{Power}} = \frac{M}{\textrm{Power}}.
\]
\begin{center}
\begin{tabular}{|l|rrr|}
\cline{2-4}
\multicolumn{1}{l|}{} & System A & System B & System C \\
\hline
Performance ($M$) & 6.6 & 12.4 & 32.0 \\
Power & 128.4 W & 145.9 W & 237.0 W \\
\hline
$G$ & 0.051 & 0.085 & 0.135 \\
$100\times G$ & 5.1 & 8.5 & 13.5 \\
\hline
\end{tabular}
\end{center}
As is evident from the table above, we recommend System C. 
System C is superior to the other systems, because it is ``greener'' by measure of performance per Watt.

\subsubsection{Recommendation 2: Best Value}
The second recommendation we make is based on a measure of performance per dollar. 
We incorporate both the up-front cost of the machine and 3 years of its energy usage. 
This metric will indicate to our client which machine is the best value, even after paying the next 3 years' electric bills.

Assume the machine operates around peak power for the 3 years, and assume an average electricity cost of \$0.12 per kWh. 
(Note: there are 26,297.4 hours in 3 years, and 1000 Watts (W) in a kiloWatt (kW).)

Define $E$ (higher $E$ indicates a better value):
\[
E = \frac{\textrm{Performance}}{\textrm{Total cost}}
= \frac{\textrm{Performance}}{\textrm{Price} + \textrm{Electricity cost}}
= \frac{M}{\textrm{Price} + 0.12\times26,297.4\times\frac{1}{1000}\times\textrm{Power}}.
\]
\begin{center}
\begin{tabular}{|l|rrr|}
\cline{2-4}
\multicolumn{1}{l|}{} & System A & System B & System C \\
\hline
Performance ($M$) & 6.6 & 12.4 & 32.0 \\
Power & 128.4 W & 145.9 W & 237.0 W \\
Price & \$433.87 & \$521.87 & \$640.92 \\
Electricity cost & \$405.19 & \$460.41 & \$747.90 \\
Total cost & \$839.06 & \$982.28 & \$1388.81 \\
\hline
$E$ & 0.00787 & 0.01262 & 0.02304 \\
$1000\times E$ & 7.87 & 12.62 & 23.04 \\
\hline
\end{tabular}
\end{center}
As is evident from the table above, we recommend System C. 
System C is superior to the other two systems, because it will yield more performance-per-dollar, even against total cost including 3 years' electricity.
